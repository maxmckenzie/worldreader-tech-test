import { expect } from 'chai'
import request from 'supertest'
import app from '../index.js'

describe('World Reader API', () => {
  describe('index', () => {
    it('should do something', done => {
      request(app)
        .get('/')
        .expect(200)
        .end((err, res) => {
          if (err) { console.log(err) }
          expect(res.text).to.equal('hello world')
          done()
        })
    })
  })

  describe('Category Tree', () => {
    it('should return the tree structure of all categories', done => {
      request(app)
        .get('/categories')
        .expect(200, done)
    })
  })

  describe('Books per category', () => {
    it('should return a list of books when a category is posted', done => {
      request(app)
        .post('/categories')
        .send({ category: 'crime' })
        .expect(200, done)
    })
  })

  describe('Book List', () => {
    it('should return a paginated list of books', done => {
      request(app)
        .get('/books?page=2&limit=25')
        .expect(200)
        .end((err, res) => {
          if (err) { console.log(err) }
          expect(res.body).to.have.lengthOf(25)
          done()
        })
    })
    it('should return UUID, Title, Author, Language, Related categories', done => {
      request(app)
        .get('/books?page=2&limit=10')
        .expect(200)
        .end((err, res) => {
          if (err) { console.log(err) }
          expect(res.body[0]).to.have.keys('id', 'uuid', 'title', 'author', 'language', 'related_categories')
          done()
        })
    })// IBAN?
    // could test the number checking as well
  })

  describe('Book Statistics', () => {
    it('should return Statistics when a book id is posted', done => {
      request(app)
        .get('/books/?id=37')
        .expect(200, done)
    })
    it('should return clients that have read this book', done => {
      request(app)
        .get('/books/?id=37')
        .expect(200)
    })
    it('should return registered users that have read the book', done => {
      request(app)
        .get('/books/?id=37')
        .expect(200)
    })
    it('should return details of how many clients have read the book in each country', done => {
      request(app)
        .get('/books/?id=37')
        .expect(200)
    })
  })

  describe('User List', () => {
    it('should return a paginated list of users', done => {
      request(app)
        .get('/users?page=2&limit=10')
        .expect(200)
        .end((err, res) => {
          if (err) { console.log(err) }
          expect(res.body).to.have.lengthOf(10)
          done()
        })
    })
    it('should optionally filter by gender', done => {
      request(app)
        .get('/users/?gender=male')
        .expect(200, done)
    })
    it('should optionally filter by age range', done => {
      request(app)
        .get('/users/?ageMin=1&ageMax=12')
        .expect(200, done)
    })
    it('should optionally sort by created', done => {
      request(app)
        .get('/users/?order=created')
        .expect(200, done)
    })
    it('should optionally sort by updated', done => {
      request(app)
        .get('/users/?order=created')
        .expect(200, done)
    })
  })
})
