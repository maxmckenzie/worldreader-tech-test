# Worldreader Tech Test

Node API to serve books from the read-only Postgres DB
[![pipeline status](https://gitlab.com/maxmckenzie/worldreader-tech-test/badges/master/pipeline.svg)](https://gitlab.com/maxmckenzie/worldreader-tech-test/commits/master)

## Install and run
I'd suggest you use yarn but of course, you can still use npm set your pg details as environment variables or use direnv and the below command to create your own envrc file https://direnv.net/

`cp .envrc-example .envrc`

then run

`yarn` or `npm i`

then `yarn start` or `npm start`

## Code Standards

[![js-standard-style](https://cdn.rawgit.com/standard/standard/master/badge.svg)](http://standardjs.com)

> The religious semicolon war 
> http://inimino.org/~inimino/blog/javascript_semicolons
> http://blog.izs.me/post/2353458699/an-open-letter-to-javascript-leaders-regarding

### JS
`yarn run lint` `npm run lint`

### Unit Tests
`yarn run test` `npm run test`

### Handover notes

- Identify the schema weaknesses (missing indices, foreign keys, ...) and propose the
modifications you would do to it.

- what is the book1 db?
- i find the has OIDs flag very worrying how are we getting these how do they enter the db without an ID?
- are you using uuids or ids
- plural and singular are all over the place

- Think of performance and scalability issues and, if any, propose some ways to
improve response times.

- Index everything!!