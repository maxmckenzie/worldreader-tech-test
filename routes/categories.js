import Router from 'express-promise-router'

const db = require('../db')
const router = new Router()

router.get('/', async (req, res) => {
  const { rows } = await db.query('SELECT * FROM category')
  res.send(rows[0])
})

module.exports = router
