import books from './books'
import categories from './categories'
import users from './users'

module.exports = (app) => {
  app.use('/books', books)
  app.use('/categories', categories)
  app.use('/users', users)
  app.use('/', (req, res) => {
    res.status(200).send('hello world')
  })
}
