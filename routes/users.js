import Router from 'express-promise-router'

const db = require('../db')
const router = new Router()

router.get('/', async (req, res) => {
  if (req.query.id) {
    const id = parseInt(req.query.id)
    const { rows } = await db.query('SELECT * FROM wr_user WHERE id = $1', [id])
    res.send(rows[0])
  } else {
    let page = 1
    const limits = [10, 25, 50, 100]

    let limit = limits[0]

    if (req.query.limit) {
      limit = parseInt(req.query.limit)
      if (isNaN(limit) || limits.indexOf(limit) === -1) limit = limits[0]
    }

    const rsPages = await db.query('SELECT COUNT(DISTINCT id) as total FROM wr_user')
    const pages = Math.ceil(rsPages.rows[0].total / limit)

    if (req.query.page) {
      page = parseInt(req.query.page)
      if (isNaN(page) || page < 1) page = 1
      if (page > pages) page = pages
    }

    const { rows } = await db.query('SELECT * FROM wr_user LIMIT $1 OFFSET $2', [limit, (page - 1) * limit])
    res.send(rows)
  }
})

module.exports = router
