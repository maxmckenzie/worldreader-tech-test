import bodyParser from 'body-parser'
import express from 'express'
import 'babel-polyfill'
// https://github.com/babel/babel/issues/6956
import mountRoutes from './routes'

const appName = 'World Reader API'
const app = express()
mountRoutes(app)

const port = process.env.PORT || 5555

app.use(bodyParser.json())

if (process.env.NODE_ENV !== 'test') {
  app.listen(port, () => {
    console.log(`${appName} listening on port ${port}...`)
  })
}

export default app
